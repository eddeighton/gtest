cmake_minimum_required(VERSION 2.8)

##assuming THIRD_PARTY_DIR

find_library( GTEST_LIB_1_DEBUG NAMES gtest.lib PATHS ${THIRD_PARTY_DIR}/gtest/build_1_7_0_vs2015/Debug )
find_library( GTEST_LIB_2_DEBUG NAMES gtest_main.lib PATHS ${THIRD_PARTY_DIR}/gtest/build_1_7_0_vs2015/Debug )
find_library( GTEST_LIB_1 NAMES gtest.lib PATHS ${THIRD_PARTY_DIR}/gtest/build_1_7_0_vs2015/Release )
find_library( GTEST_LIB_2 NAMES gtest_main.lib PATHS ${THIRD_PARTY_DIR}/gtest/build_1_7_0_vs2015/Release )

find_path( GTEST_INCLUDE_DIRS NAMES gtest/gtest.h PATHS ${THIRD_PARTY_DIR}/gtest/googletest/include )

include_directories( ${GTEST_INCLUDE_DIRS} )

function( link_gtest targetname )
	target_link_libraries( ${targetname} debug ${GTEST_LIB_1_DEBUG} optimized ${GTEST_LIB_1} )
endfunction( link_gtest )
